import { BitcoinData } from './modal';

const bitcoinData = BitcoinData.create({
    sequence: "",
    addr: "",
    value: ""
});

const rootStore = RootStore.create({
    bitcoinSequence: [bitcoinData]
});