import { types } from "mobx-state-tree";

export const BitcoinData = types.model('bitcoinData', {
    sequence: types.identifier,
    addr: types.string,
    value: types.identifier
}).actions(self => ({
    setSequence(bitcoin) {
        self.sequence: bitcoin.sequence,
            self.addr: bitcoin.addr,
                self.value: bitcoin.value
    }
}).views(self => ({
    getSequence() {
        return {
            sequence,
            addr,
            value
        }
    }
})));