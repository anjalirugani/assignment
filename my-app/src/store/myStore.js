import { BitcoinData } from './modal';


export const RootStore = types.model({
    bitcoinSequence: types.array(BitcoinData)
})
