import logo from './logo.svg';
import './App.css';
import { Main } from './master-component/MainComponent';
import { MasterComponent } from './master-component/masterComponent';

function App() {
  return (
    <MasterComponent/>
  );
}

export default App;
