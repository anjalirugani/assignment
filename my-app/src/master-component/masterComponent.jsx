import React, { useEffect, useState } from "react";
import { types } from "mobx-state-tree";
import { observer } from 'mobx-react-lite'
import { values } from 'mobx'

const apiCallSubscribe = {
    "op": "unconfirmed_sub"
}

const apiCallUnsubscribe = {
    "op": "unconfirmed_unsub"
}

export const MasterComponent = () => {

    const [connect, setConnect] = useState(false);
    const [disconnect, setDisconnect] = useState(false)
    const [addr, setAddr] = useState([]);
    const [value, setValue] = useState([]);
    const [sequence, setSequence] = useState([]);

    useEffect(() => {
        const ws = new WebSocket('wss://ws.blockchain.info/inv');
        if (connect) {
            ws.onopen = (event) => {
                ws.send(JSON.stringify(apiCallSubscribe));
                console.log('event', event)
            };
            let seqArray = []
            ws.onmessage = function (event) {
                const json = JSON.parse(event.data);
                console.log('json->', json)
                try {
                    if (json.x.inputs.length > 0) {
                        json.x.inputs.map((item) => {
                            let seqObj = {
                                [item.prev_out.sequence]: {
                                    addr: item.prev_out.addr,
                                    val: item.prev_out.value
                                }
                            }
                            if (seqArray.length < 10) {
                                seqObj && seqArray.push(seqObj);
                            } else {
                                ws.close()
                            }
                        })
                    }

                }
                catch (err) {
                    console.log(err);
                }
            };
            setSequence(seqArray);
        } else if (disconnect) {
            console.log('here---')
            // ws.onopen = (event) => {
            //     ws.send(JSON.stringify(apiCallUnsubscribe));
            //     console.log('event', event)
            // };
            // ws.onmessage = function (event) {
            //     const json = JSON.parse(event.data);
            //     console.log('json------->', json.x.inputs[0])
            //     try {
            //         const addrArray = [];
            //         const sequenceArray = [];
            //         const valArray = [];
            //         if (json.x.inputs.length > 0) {
            //             json.x.inputs.map((item, i) => {
            //                 if (i < 10) {
            //                     addrArray.push(item.prev_out.addr);
            //                     sequenceArray.push(item.prev_out.sequence);
            //                     valArray.push(item.prev_out.value)
            //                 } else {
            //                     ws.close()
            //                 }
            //             })
            //             setSequence(sequenceArray);
            //             setAddr(addrArray);
            //             setValue(valArray)
            //         }
            //     } catch (err) {
            //         console.log(err);
            //     }
            // };
            ws.close();
        }

    });

    console.log('connect---', sequence)
    return (
        <div style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }}>
            <div style={{ flex: 1, justifyContent: 'center' }}>
                <h2>transaction</h2>
                <table style={{ textAlign: 'center', fontFamily: "Trebuchet MS", borderCollapse: 'collapse', border: '3px solid #ddd', width: '50%' }}>
                    <tbody>
                        <tr>
                            <th style={{ border: '1px solid #ddd', padding: '8px' }}>Address</th>
                            <th style={{ border: '1px solid #ddd', padding: '8px' }}>Value</th>
                        </tr>
                        {/* {
                            sequence.length && sequence.map((currentVal) => {
                                <tr>
                                    <td style={{ border: '1px solid #ddd', padding: '8px' }}>{currentVal.addr}</td>
                                    <td style={{ border: '1px solid #ddd', padding: '8px' }}>{currentVal.value}</td>
                                </tr>
                            })
                        } */}
                    </tbody>
                </table>
            </div>
            <div style={{ flex: 1, justifyContent: 'center' }}>
                <button style={{ flex: 1, margin: 30, padding: 10 }} onClick={() => {
                    setConnect(true);
                    setDisconnect(false);
                }}>Connect</button>
                <button style={{ flex: 1, margin: 30, padding: 10 }} onClick={() => {
                    setConnect(false);
                    setDisconnect(true);
                }}>Disconnect</button>
            </div>
        </div >
    )


}