import { Component, createContext } from "react"
import { ChildComponent } from "./ChildComponent";

export class Main extends Component {

    constructor(props) {
        super(props);
        this.ws = new WebSocket('wss://ws.blockchain.info/inv');

        const MyContext = createContext();
    }


    componentDidMount() {
        const op = {
            "op": "addr_sub",
            "addr": "$bitcoin_address"
        }

        this.ws.onopen = (event) => {
            this.ws.send(op);
        }


        this.ws.onmessage = function (event) {
            const json = JSON.parse(event.data);
            console.log(`[message] Data received from server: ${json}`);
            try {
                if ((json.event = "data")) {

                    console.log(json.data);
                }
            } catch (err) {
                // whatever you wish to do with the err
            }

        };


        this.ws.onclose = () => {
            console.log('disconnected');

        }

    }

    render() {
        console.log('this--', this.ws)
        return (
            <ChildComponent websocket={this.ws} />
        )
    }
}